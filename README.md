# interview-take-home-task

Middleware web service that allow IVR application to communicate with payment processor and return the results of the transaction to the IVR application.
(THIS REPO ITS ONLY FOR TESTING PURPOSES)

## Installation 

Clone this repository

	$ git clone https://bitbucket.org/kangu_arrovi/interviewtakehometask.git

Create a virtualenv (on Debian-based Linux)

    $ cd interviewtakehometask
	$ python3 -m venv env
	$ source env/bin/activate

Upgrade pip if it is required 

	$ pip install --upgrade pip

Install requirements 

	$ pip install -r req.txt
	
MySQL Config

In your MySQL instance, run this script:

	$ create database IVRappdb;
	
Change settings.py

	$ cd interviewtakehometask
	$ nano settings.py

	DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.mysql',
			'NAME': 'IVRappdb',
			'USER': 'root', #Your mysql user.
			'PASSWORD': 'root', #Your mysql password
			'HOST': 'localhost',
			'PORT': '3306',
		}
	}
	
	STRIPE_SECRET_KEY = '<your stripe secret key for testing>'
	STRIPE_PUBLISHABLE_KEY = '<your stripe publichable key for testing>'
	
Make database migrations

	$ python manage.py makemigrations
	$ python manage.py migrate
	
## Run in development

	$ python manage.py runserver

    Go to the browser at 127.0.0.1:8000 and begin to use it.
	
## Test with curl 
(You may go to http://127.0.0.1:8000/ in your browser and test it with DRF View)

Note: amount keeps the cents value format.

	$ curl -d '{"cc_num": "4111111111111111", "cvv": "123", "exp_date": "1219", "trans_id": "321654351687", "amount":"100"}' -H "Content-Type: application/json" -X POST http://127.0.0.1:8000/

To see the tests you must go to https://dashboard.stripe.com/test/payments in your Stripe acount.

![Test1](images/test1.png)

To see the saved information of all Requests and Responses in MySQL, you must run this scripts in your mysql shell.

    $ use IVRappdb;
    $ select * from middlewareweb_ivrrequests;
    $ select * from middlewareweb_ivrresponses;



