from django.apps import AppConfig


class MiddlewarewebConfig(AppConfig):
    name = 'middlewareweb'
