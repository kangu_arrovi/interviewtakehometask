import stripe
from django.conf import settings
from rest_framework import serializers
from .models import IVRRequests, IVRResponses

stripe.api_key = settings.STRIPE_SECRET_KEY

class IVRRequestsSerializer(serializers.ModelSerializer):

	def __init__(self, charge=None, status_code=None, *args, **kwargs):
		super(IVRRequestsSerializer, self).__init__(*args, **kwargs)
		self.charge = charge #This will save the response of the charge
		self.status_code = status_code #This will save the response of the charge

	class Meta:
		model = IVRRequests
		fields = ('id', 'cc_num', 'cvv', 'exp_date', 'trans_id', 'amount')

	def create(self, validated_data):

		def make_charge(data):
			try:
				token = stripe.Token.create(
					card={
						"number": data['cc_num'],
						"exp_month": int(data['exp_date'][0:2]),
						"exp_year": int('20' + data['exp_date'][2:5]),
						"cvc": data['cvv']
					}
				)

				charge = stripe.Charge.create(
					amount=data['amount'],
					currency='usd',
					description='MiddlewareIVR-Charge',
					source=token,
				)
				self.status_code = 200
				return charge

			except stripe.error.CardError as e:
				error = e.json_body
				self.status_code = e.http_status
				return error

			except stripe.error.RateLimitError as e:
				error = e.json_body
				self.status_code = e.http_status
				return error

			except stripe.error.InvalidRequestError as e:
				error = e.json_body
				self.status_code = e.http_status
				return error

			except stripe.error.AuthenticationError as e:
				error = e.json_body
				self.status_code = e.http_status
				return error

			except stripe.error.APIConnectionError as e:
				error = e.json_body
				self.status_code = e.http_status
				return error

			except stripe.error.StripeError as e:
				error = e.json_body
				self.status_code = e.http_status
				return error

		charge = make_charge(validated_data)

		validated_data['cvv'] = 'X'*3
		validated_data['cc_num'] = 'X'*12 + validated_data['cc_num'][12:19]
		response = IVRRequests.objects.create(**validated_data)

		IVRResponses.objects.create(response=str(charge), request=response) #Charge is stringify

		self.charge=charge
		return response