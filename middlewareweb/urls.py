from django.urls import path
from middlewareweb import views

urlpatterns = [
    path('', views.IVRRequestsView.as_view()),
]