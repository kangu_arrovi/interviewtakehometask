from django.db import models

class IVRRequests(models.Model):
	"""
	This models saves every IVR request.
	"""
	id = models.AutoField(blank=False, null=False, primary_key=True)
	cc_num = models.CharField(max_length=16)
	cvv = models.CharField(max_length=3)
	exp_date = models.CharField(max_length=5)
	trans_id = models.CharField(max_length=100)
	amount = models.CharField(max_length=50)
	created = models.DateTimeField(auto_now_add=True) #Control purposes

	class Meta:
		ordering = ('created',)

class IVRResponses(models.Model):
	"""
	This models saves every Stripe response. Each one is asigned to their own previous request.
	"""
	id = models.AutoField(blank=False, null=False, primary_key=True)
	response = models.TextField() #The response is saved as a stringify JSON.
	request = models.ForeignKey(IVRRequests, on_delete=models.CASCADE)
	created = models.DateTimeField(auto_now_add=True) #Control purposes
	
	class Meta:
		ordering = ('created',)