from .models import IVRRequests
from .serializers import IVRRequestsSerializer
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class IVRRequestsView(APIView):

	serializer_class = IVRRequestsSerializer

	def post(self, request, format=None):
		serializer = IVRRequestsSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
			return Response(serializer.charge, status=serializer.status_code)
		return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)